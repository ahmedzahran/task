<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
use System\User\Models\UserGroup;
use System\User\Models\User;
use System\Company\Models\Company;
//$faker->randomElement($materials)
$factory->define(UserGroup::class, function (Faker $faker) {
    return [
    	  'name' => $faker->name,
    ];
});

$factory->define(Company::class, function (Faker $faker) {
    return [
    	  'name' => $faker->name,
    	  'tel' => $faker->phoneNumber,
    	  'address' => $faker->streetAddress,
    	  'email' => $faker->unique()->safeEmail,
    ];
});

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'user_group_id' => $faker->numberBetween($min = 1,$max = 2),
        'company_id' => $faker->numberBetween($min = 1,$max = 30),
        'remember_token' => str_random(10),
    ];
});
