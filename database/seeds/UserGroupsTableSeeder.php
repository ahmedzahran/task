<?php

use System\User\Models\UserGroup;
use Illuminate\Database\Seeder;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userGroups = factory(UserGroup::class,2)->create();

        /*
		* Filter to add the first raw as admin user_group
        */
        $userGroups->filter(function($userGroup,$k){
			
			return $k == 0;

        })->map(function($admin){
        	return $admin->update([
        		'name' => 'admin'
        	]);
        });

        /*
		* Filter to add the second raw as employee user_group
        */
        $userGroups->filter(function($userGroup,$k){
			return $k != 0;

        })->map(function($employee){
        	return $employee->update([
        		'name' => 'employee'
        	]);
        });
      
    }
}
