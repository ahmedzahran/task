<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('user_group_id','userGroupID')
                ->references('id')
                ->on('user_groups')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('company_id','companyID')
                ->references('id')
                ->on('companies')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->dropForeign('userGroupID');
            $table->dropForeign('companyID');
        });
    }
}
