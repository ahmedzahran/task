<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use System\Company\Models\Company;
use System\User\Models\User;

class CompaniesTest extends TestCase
{
  
  /*@test */
  public function test_company_consist_of_employees()
  {
  		$company = create(Company::class);

  		$emplaoyee = create(User::class,[
  			'user_group_id' =>2,
  			'company_id' => $company->id
  		]);

  		$this->assertTrue($company->employees->contains($emplaoyee));

  }
}
