<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use System\Company\Models\Company;
use System\User\Models\User;
use Tests\TestCase;

class EmplaoyeesTest extends TestCase
{
 

 	public function test_employee_belong_to_company()
 	{
 		$company = create(Company::class);

 		$employee = create(User::class,[
 			'company_id' => $company->id
 		]);

 		$this->assertInstanceOf(Company::class,$employee->company);
 	}


 
}
