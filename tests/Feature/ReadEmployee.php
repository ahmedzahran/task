<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use System\Company\Models\Company;
use System\User\Models\User;
use Tests\TestCase;

class ReadEmployee extends TestCase
{

	
    /*@ test*/
    public function test_admin_user_can_delete_employee_in_certain_company()
    {

        $admin = $this->signIn(create(User::class,['user_group_id' => 1]));

        
        $company = create(Company::class);


        $response = $this->post('/dashboard/companies/',$company->toArray());

        $r = $this->get(route('companies.show',$company->id))
            ->assertSee($company->name);
    }

}
