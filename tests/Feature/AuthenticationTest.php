<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use System\User\Models\User;

class AuthenticationTest extends TestCase
{
    /*@test */

    public function test_employee_user_redirect_after_login()
    {
    	$employee = create(User::class,[
    		'user_group_id' => 2,
    		'password' => bcrypt($password = 'i-love-laravel')
    	],1);

    	 $response = $this->post('/login',[
    		'email' => $employee[0]->email,
    		'password' => $password
    	]);


    	 $response->assertRedirect(route('employees.profile'));

   	}

   	/*@ test*/
   	public function test_admin_user_redirect_after_login()
   	{
   		$admin = create(User::class,[
   			'user_group_id' => 1,
   			'password' => bcrypt($password = 'iam-admin')
   		],1);

   		$response = $this->post('/login',[
   			'email' => $admin[0]->email,
   			'password' => $password
   		]);

   		$response->assertRedirect('/dashboard/');
   	}
}
