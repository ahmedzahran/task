<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use System\Company\Models\Company;
use System\User\Models\User;
use Tests\TestCase;

class ReadCompanyTest extends TestCase
{

	/*@test*/
	public function test_unauthenticated_user_cannot_create_company()
	{
		create(User::class);

		$this->get('/dashboard/companies/create')
		->assertRedirect('login');
	}


    /*@ test*/
    public function test_authenticated_employee_user_cannot_create_company()
    {
    	$user = create(User::class,['user_group_id' => 2]);

    	$this->signIn($user);

    	$this->get('/dashboard/companies/create')
    	->assertRedirect('login');
    }


    /* @test*/
    public function test_admin_can_filter_company_by_name()
    {
        $this->signIn(create(User::class,['user_group_id'=>1]));

        $company = create(Company::class,['name'=>'test']);

        $companyNotMatchTheFilter = create(Company::class);

        $this->get('/dashboard/companies?name=test')
             ->assertSee($company->name == 'test')
             ->assertDontSee($companyNotMatchTheFilter);


    }

    /*@ test*/
    public function test_company_require_name()
    {
        $this->signIn(create(User::class,['user_group_id' => 1]));
        $company = make(Company::class,['name' => null]);

        $this->post('/dashboard/companies/',$company->toArray())
            ->assertSessionHasErrors('name');

    }

    /*@ test*/
    public function test_authenticated_admin_user_can_create_company()
    {

        $admin = $this->signIn(create(User::class,['user_group_id' => 1]));

        $company = create(Company::class);

        $response = $this->post('/dashboard/companies/',$company->toArray());

        $r = $this->get(route('companies.show',$company->id))
            ->assertSee($company->name);
    }


    /*@ test*/
    public function test_admin_user_cannot_delete_company_that_has_employees()
    {

        $admin = $this->signIn(create(User::class,['user_group_id' => 1]));

        
        $company = create(Company::class);

        $employee = create(User::class,['company_id'=>$company->id]);

        $response = $this->delete(route('companies.destroy',$company->id),$company->toArray());

        $this->get(route('companies.index'))
            ->assertSee('not valid delete');
    }


    /*@ tets*/
    public function test_admin_user_can_delete_company_that_hasnot_employee()
    {
        $admin = $this->signIn(create(User::class,['user_group_id' => 1]));


        $company = create(Company::class);

        $this->delete(route('companies.destroy',$company->id),$company->toArray());

        $this->assertDatabaseMissing('companies',['id' => $company->id]);
    }


}
