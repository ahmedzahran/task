@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>company details</h1>

@stop


@section('content')
	<div class="box">
		<div class="box-header">
			<h3>{{$company->name}}</h3>
		</div>
		<div class="box-body">
			<h2>Employees</h2>
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
			  Assign Employees to this Company
			</button>


			<table class="table table-striped">
				<thead>
					<tr>
						<th>id</th>
						<th>name</th>
						<th>email</th>
						<th>delete</th>
					</tr>
				</thead>
				<tbody>
					@foreach($company->employees as $k=>$employee)
						<tr>
							<td>{{$k+1}}</td>
							<td>{{$employee->name}}</td>
							<td>{{$employee->email}}</td>
							<td>
								{{-- <form action="{{route('employees.detach.company',$employee->id)}}" method="post">
									{{method_field('DELETE')}}
									{{csrf_field()}}
									<button type="submit" class="btn btn-danger">delete user</button>
									
								</form> --}}
								<a class="btn btn-danger" href="{{route('employees.detach.company',[$company->id,$employee->id])}}">detach</a>

							</td>
						</tr>
					@endforeach;
				</tbody>
			</table>

		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form method="post" action="{{route('companies.attach.employees',$company->id)}}">
	        	{{csrf_field()}}
	        	<div class="form-group">
	        		<select id="employees" multiple="multiple" class="form-control" name="employees[]">
	        		</select>
	        	</div>
	        		     <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
	        </form>
	      </div>

	    </div>
	  </div>
	</div>
@stop




@section('js')

    <script>
    	
    $('#exampleModal').on('shown.bs.modal', function () {
    	$.ajax({
	    	type: 'get',
	    	url : "{{route('employees.api.index')}}",
	    	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

	    	success: function(res){
	    		$.each(res.data,function(i,item){
	    			$('#employees').append("<option value="+item.id+">"+item.name+"</option>");
	    		});
	    	}
    	});
	});



    </script>

@stop