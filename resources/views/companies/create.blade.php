@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>create company</h1>

@stop


@section('content')
	@include('partials.Errors')
	<form method="post" action="{{route('companies.store')}}">
		{{csrf_field()}}
		<div class="form-group">
			<label>name</label>
			<input type="text" name="name" value="{{old('name')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>tel</label>
			<input type="text" name="tel" value="{{old('tel')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>address</label>
			<input type="text" name="address" value="{{old('address')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>email</label>
			<input type="text" name="email" value="{{old('email')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>employees</label>

			<select name="employees[]" class="form-control" multiple>
				<option selected="selected" value=""></option>
                @forelse($employees as $employee)
					<option value="{{$employee->id}}">{{$employee->name}}</option>
                   @empty
                    <option value="{{null}}">no data</option>
                @endforelse
            </select>
		</div>
		<button class="btn btn-primary" type="submit">create</button>
	</form>

@stop




