@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>companies lisr</h1>

@stop


@section('content')

	<div class="box">
		<div class="box-header">
				@include('partials.Errors')
				@include('flash::message')
			<form method="GET" action="/dashboard/companies">
				
				<div class="form-group">
					<label>name</label>
					<input type="text" value="{{request('name')}}" name="name">
				</div>
				<button type="submit" class="btn btn-primary">search</button>
			</form>
		</div>
		<div class="box-body">
			<a href="{{route('companies.create')}}" class="btn btn-primary">create</a>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>number</th>
						<th>name</th>
						<th>action</th>
					</tr>
				</thead>
				<tbody>
					@forelse($companies as $k=>$company)
						<tr>
							<td>{{$k+1}}</td>
							<td><a href="{{route('companies.show',$company->id)}}">{{$company->name}}</a></td>
							<td>
								<a href="{{route('companies.edit',$company->id)}}" class="btn btn-primary-xs">edit</a>
								<form action="{{route('companies.destroy',$company->id)}}" method="post">
									{{method_field('DELETE')}}
									{{csrf_field()}}
									<button type="submit" class="btn btn-danger">delete</button>
									
								</form>
							</td>
						</tr>
						@empty
						<tr><td colspan="3">No data</td></tr>
					@endforelse
				</tbody>
			</table>
			@if(!$companies->isEmpty())
				@include('partials.PaginationLinks',['data'=>$companies])
			@endif
		</div>
	</div>
@stop





@section('js')

    <script> 

    	$('#flash-overlay-modal').modal();

    	$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

    </script>

@stop