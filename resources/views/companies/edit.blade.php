@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>edit company</h1>

@stop


@section('content')
	@include('partials.Errors')
	<form method="post" action="{{route('companies.update',$company->id)}}">
		{{method_field('PATCH')}}
		{{csrf_field()}}
		<div class="form-group">
			<label>name</label>
			<input type="text" name="name" value="{{$company->name}}" class="form-control">
		</div>
		<div class="form-group">
			<label>tel</label>
			<input type="text" name="tel" value="{{$company->tel}}" class="form-control">
		</div>
		<div class="form-group">
			<label>address</label>
			<input type="text" name="address" value="{{$company->address}}" class="form-control">
		</div>
		<div class="form-group">
			<label>email</label>
			<input type="text" name="email" value="{{$company->email}}" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">create</button>
	</form>

@stop




