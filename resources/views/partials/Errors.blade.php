 @if ($errors->any())
     @foreach ($errors->all() as $error)
         <p class="btn btn-danger">{{$error}}</p>
     @endforeach
 @endif