@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>create employee</h1>

@stop


@section('content')
	@include('partials.Errors')
	<form method="post" action="{{route('employees.store')}}">
		{{csrf_field()}}
		<div class="form-group">
			<label>name</label>
			<input type="text" name="name" value="{{old('name')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>phone</label>
			<input type="text" name="phone" value="{{old('phone')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>password</label>
			<input type="password" name="password"  class="form-control">
		</div>
		<div class="form-group">
			<label>email</label>
			<input type="text" name="email" value="{{old('email')}}" class="form-control">
		</div>
		<div class="form-group">
			<label>companies</label>

			<select name="company_id" class="form-control">
                @forelse($comanies as $company)
					<option value="{{$company->id}}">{{$company->name}}</option>
                   @empty
                    <option value="{{null}}">no data</option>
                @endforelse
            </select>
		</div>
		<button class="btn btn-primary" type="submit">create</button>
	</form>

@stop




