@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>employees list</h1>

@stop


@section('content')

	<div class="box">
		<div class="box-header">
				@include('partials.Errors')
				@include('flash::message')
			<form method="GET" action="/dashboard/employees">
				
				<div class="form-group">
					<label>name</label>
					<input type="text" value="{{request('name')}}" name="name">
				</div>
				<div class="form-group">
					<label>company</label>
					<input type="text" value="{{request('company')}}" name="company">
				</div>
				<button type="submit" class="btn btn-primary">search</button>
			</form>
		</div>
		<div class="box-body">
			<a href="{{route('employees.create')}}" class="btn btn-primary">create</a>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>number</th>
						<th>name</th>
						<th>company</th>
						<th>action</th>
					</tr>
				</thead>
				<tbody>
					@forelse($employees as $k=>$employee)
						<tr>
							<td>{{$k+1}}</td>
							<td><a href="{{route('employees.show',$employee->id)}}">{{$employee->name}}</a></td>
							<td>{{optional($employee->company)->name}}</td>
							<td>
								<a href="{{route('employees.edit',$employee->id)}}" class="btn btn-primary-xs">edit</a>
								<form action="{{route('employees.destroy',$employee->id)}}" method="post">
									{{method_field('DELETE')}}
									{{csrf_field()}}
									<button type="submit" class="btn btn-danger">delete</button>
									
								</form>
							</td>
						</tr>
						@empty
						<tr><td colspan="3">No data</td></tr>
					@endforelse
				</tbody>
			</table>
			@if(!$employees->isEmpty())
				@include('partials.PaginationLinks',['data'=>$employees])
			@endif
		</div>
	</div>
@stop





@section('js')

    <script> 

    	$('#flash-overlay-modal').modal();

    	$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

    </script>

@stop