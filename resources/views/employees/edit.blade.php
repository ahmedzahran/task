@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>edit employee</h1>

@stop


@section('content')
	@include('partials.Errors')
	<form method="post" action="{{route('employees.update',$employee->id)}}">
		{{method_field('PATCH')}}

		{{csrf_field()}}
		<div class="form-group">
			<label>name</label>
			<input type="text" name="name" value="{{$employee->name}}" class="form-control">
		</div>
		<div class="form-group">
			<label>phone</label>
			<input type="text" name="phone" value="{{$employee->phone}}" class="form-control">
		</div>
		<div class="form-group">
			<label>password</label>
			<input type="password" name="password"  class="form-control">
		</div>
		<div class="form-group">
			<label>email</label>
			<input type="text" name="email" value="{{$employee->email}}" class="form-control">
		</div>
		<div class="form-group">
			<label>companies</label>

			<select name="company_id" class="form-control">
				<option value="{{optional($employee->company)->id}}" selected="selected">{{optional($employee->company)->name}}</option>
                @forelse($companies as $company)
                	@if($company->id != optional($employee->company)->id)
					<option value="{{$company->id}}">{{$company->name}}</option>
					@endif
                   @empty
                    <option value="{{null}}">no data</option>
                @endforelse
            </select>
		</div>
		<button class="btn btn-primary" type="submit">create</button>
	</form>

@stop