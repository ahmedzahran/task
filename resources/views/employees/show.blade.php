@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content_header')

    <h1>employee details</h1>

@stop


@section('content')
	<div class="box">
		<div class="box-header">
			<h3>{{$employee->name}}</h3>
		</div>
		<div class="box-body">
			<h2>company</h2>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>id</th>
						<th>name</th>
						<th>email</th>
					</tr>
				</thead>
				<tbody>
						<tr>
							<td>{{optional($employee->company)->name}}</td>
							<td>{{optional($employee->company)->email}}</td>
						</tr>
				</tbody>
			</table>

		</div>
	</div>
@stop




