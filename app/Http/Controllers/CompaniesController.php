<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Requests\StoreCompanyRequest;
use Illuminate\Http\Request;
use System\Company\Contracts\CompanyRepoContract;
use System\Company\Filters\CompanyFilters;
use System\Company\Repositories\CompanyRepository;
use System\User\Contracts\EmployeeRepoContract;

class CompaniesController extends Controller
{
	protected $companyRepo;
    protected $employeeRepo;

	public function  __construct(CompanyRepoContract $repo,EmployeeRepoContract $empRepo)
	{
		$this->companyRepo = $repo;
        $this->employeeRepo = $empRepo;
	}

    public function index(CompanyFilters $filters)
    {
    	$companies =  $this->companyRepo->all($filters);
    	return view('companies.list',compact('companies'));
    }


    public function create()
    {
    	$employees = $this->employeeRepo->getManyBy('user_group_id','2');
    	return view('companies.create',compact('employees'));
    }

    public function show($company_id)
    {
        $company = $this->companyRepo->getById($company_id,['employees']);

        return view('companies.show',compact('company'));
    }

    public function store(StoreCompanyRequest $request)
    {
        $request->validated();
    
        $company = $this->companyRepo->StoreCompany(request());

        return redirect()->route('companies.show',$company->id);
    }

    public function edit($company_id)
    {
        $company = $this->companyRepo->getById($company_id);

        return view('companies.edit',compact('company'));
    }

    public function update(CompanyUpdateRequest $request,$id)
    {
        $request->validated();

        $this->companyRepo->updateCompany($id,request());

        flash('company updated success');

        return redirect()->route('companies.index');
    }
    public function destroy($company_id)
    {
        $company =  $this->companyRepo->getById($company_id,['employees']);

        if ($company->employees->count() > 0) {

             flash('not valid delete')->error();

             return back();
        }

        $company->delete();

        flash('company delted success')->success();

        return redirect()->back();
    }


    public function attachEmployeesToCompany($company_id)
    {

        $company = $this->companyRepo->getById($company_id);

         $this->companyRepo->attachEmployees(request('employees'),$company);

        return redirect()->route('companies.show',$company->id);
    }
    
}
