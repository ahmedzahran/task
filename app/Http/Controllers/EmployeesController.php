<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeUpdateRequest;
use App\Http\Requests\StoreEmployeeRequest;
use Illuminate\Http\Request;
use System\Company\Contracts\CompanyRepoContract;
use System\User\Contracts\EmployeeRepoContract;
use System\User\Filters\EmployeeFilters;

class EmployeesController extends Controller
{

	protected $companyRepo;
    protected $employeeRepo;

	public function  __construct(CompanyRepoContract $repo,EmployeeRepoContract $empRepo)
	{

		$this->companyRepo = $repo;
        $this->employeeRepo = $empRepo;
	}
	public function index(EmployeeFilters $filters)
	{
		$employees =  $this->employeeRepo->all($filters);
    	return view('employees.list',compact('employees'));
	}


	public function create()
    {
    	$comanies = $this->companyRepo->getList(['id','name']);

    	return view('employees.create',compact('comanies'));
    }

    public function store(StoreEmployeeRequest $request)
    {
        $request->validated();

        $employee = $this->employeeRepo->storeEmployee(request());

        return redirect()->route('employees.show',$employee->id);

    }

    public function show($empId)
    {
    	$employee = $this->employeeRepo->getById($empId,['company']);

    	return view('employees.show',compact('employee'));
    }

    public function edit($empId)
    {
    	$employee = $this->employeeRepo->getById($empId,['company']);

    	$companies = $this->companyRepo->getList(['id','name']);

    	return view('employees.edit',compact('employee','companies'));

    }

    public function update(EmployeeUpdateRequest $request,$id)
    {
        $request->validated();

        $this->employeeRepo->updateEmployee($id,request());

        flash('employee updated success');

        return redirect()->route('employees.index');
    }

    public function destroy($id)
    {
    	$employee = $this->employeeRepo->getById($id);

    	if (auth()->id() == $id) {
    		flash('user can`t delete him self')->error();
    		return redirect()->route('employees.index');
    	}

    	$employee->delete();

    	flash('employee deleted success')->success();

    	return redirect()->route('employees.index');

    }


    public function detachEmployee($company_id,$emplaoyee_id)
    {
        $company = $this->companyRepo->getById($company_id);
        $employee = $this->employeeRepo->getById($emplaoyee_id);

        $this->employeeRepo->detachEmployeeFromCompany($employee,$company);

        return redirect()->back();

    }
}
