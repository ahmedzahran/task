<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use System\User\Contracts\EmployeeRepoContract;

class ApiEmployeesController extends Controller
{
    
	protected $empRepo;

    public function __construct(EmployeeRepoContract $emp)
    {
    	$this->empRepo = $emp;	
    }


    public function index()
    {
    	$data = $this->empRepo->getList(['id','name']);
    	return response()->json(['data' => $data]);
    }
}
