<?php

namespace System\User\Filters;
use System\Infrastructure\QueryFilter;

class EmployeeFilters extends QueryFilter
{

	public function name($name = '')
	{
		return $this->builder->where('name','LIKE',"%$name%");
	}

	public function company($company = '')
	{
		return $this->builder->whereHas('company',function($q) use($company){
			$q->where('name','LIKE',"%$company%");
		});
	}
}


