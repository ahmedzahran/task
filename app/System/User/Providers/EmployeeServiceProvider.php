<?php

namespace System\User\Providers;

use Illuminate\Support\ServiceProvider;
use System\User\Contracts\EmployeeRepoContract;
use System\User\Repositories\EmployeesRepository;

class EmployeeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmployeeRepoContract::class,EmployeesRepository::class);
    }
}
