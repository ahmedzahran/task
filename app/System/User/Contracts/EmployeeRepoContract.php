<?php

namespace System\User\Contracts;

interface EmployeeRepoContract 
{
	public function storeEmployee($data);
	public function updateEmployee($id,$data);
	public function detachEmployeeFromCompany($emp,$comp);
}