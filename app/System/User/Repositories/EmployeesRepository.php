<?php

namespace System\User\Repositories;
use System\Infrastructure\Repository;
use System\User\Contracts\EmployeeRepoContract;
use System\User\Models\User;
use System\User\Models\UserGroup;

class EmployeesRepository extends Repository implements EmployeeRepoContract
{

	protected $model;

	public function __construct(User $employee)
	{
		$this->model = $employee;
	}


	public function storeEmployee($request)
	{
		return $this->EmployeeUserGroup()
		->users()
		->create([
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'company_id' => $request->company_id,
			'password' => bcrypt($request->password)
		]);

	}

	public function updateEmployee($id,$request)
	{
		$emp = $this->getById($id);

		$emp->update([
			'name' => $request->name,
			'phone' => $request->phone,
			'email' => $request->email,
			'company_id' => $request->company_id
		]);

		if ($request->has('password')) {

			$emp->update([
				'password' =>bcrypt($request->password)
			]);
		}

		return $emp;
	}

	private function EmployeeUserGroup()
	{
		return UserGroup::where('id',2)->first();
	}


	public function detachEmployeeFromCompany($emp,$comp)
    {
        return $emp->company()->dissociate($comp->id)->save();
    }
}