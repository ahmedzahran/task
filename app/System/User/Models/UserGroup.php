<?php

namespace System\User\Models;

// use Illuminate\Database\Eloquent\Model;

use System\Infrastructure\Model;
use System\User\Models\User;

class UserGroup extends Model
{
    
    protected $guarded = [];


    public function users()
    {
    	return $this->hasMany(User::class,'user_group_id');
    }
}
