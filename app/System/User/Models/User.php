<?php

namespace System\User\Models;

use System\Company\Models\Company;
use System\Infrastructure\Model;

class User extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','company_id','phone'
    ];

    protected $guarded = ['user_group_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function company()
    {
        return $this->belongsTo(Company::class,'company_id');
    }
}
