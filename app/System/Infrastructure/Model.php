<?php

namespace System\Infrastructure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use System\Infrastructure\Filterable;

abstract class Model extends Authenticatable
{

	use Notifiable;

	use Filterable;
}