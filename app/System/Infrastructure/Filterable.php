<?php

namespace System\Infrastructure;

use System\Infrastructure\QueryFilter;

trait Filterable 
{

	/**
     * Filter a result set.
     *
     * @param  Builder      $query
     * @param  QueryFilter $filters
     * @return Builder
    */
	public function scopeFilter($query, QueryFilter $filters)
	{
	    return $filters->apply($query);
	}
}