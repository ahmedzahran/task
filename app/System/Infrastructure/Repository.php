<?php

namespace System\Infrastructure;


abstract class Repository
{

    protected $createdModel;

    /**
     * Get all model
     * @param array $with
     * @param bool $paginate
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all($filters = [],$with = [])
    {

        $q = $this->make($with)->latest()->filter($filters);

       $query = $q->paginate(20);

       $query->appends(request()->except(['page']));

       return $query;
    }

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = array())
    {

        $query = $this->make($with);
        return $query->findOrFail($id);
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     */
    public function make(array $with = array())
    {
        return $this->model->with($with);
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array())
    {
       return  $this->make($with)->where($key, '=', $value)->first();
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array())
    {
        return $this->make($with)->where($key, '=', $value)->get();
    }

    /**
     * Return all results that have a required relationship
     *
     * @param string $relation
     */
    public function has($relation, array $with = array())
    {
        $entity = $this->make($with);

        return $entity->has($relation)->get();
    }



    /**
     * Return the required columns in array
     * @param array $columns
     * @return array of columns
     */
    public function getList(array $columns)
    {
        return $this->model->all($columns);
    }

    /*
     * Delete Entity
     * @param EntityId
     * @param UserId
     */
    public function delete($entityId)
    {
        $entity = $this->getById($entityId);

        $entity->delete();

        return true;
    }

}