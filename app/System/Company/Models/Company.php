<?php

namespace System\Company\Models;

// use Illuminate\Database\Eloquent\Model;
use System\Infrastructure\Model;
use System\User\Models\User;

class Company extends Model
{

	protected $guarded = [];

	public function employees()
	{
		return $this->hasMany(User::class,'company_id');
	}

}
