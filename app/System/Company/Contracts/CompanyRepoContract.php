<?php

namespace System\Company\Contracts;

interface CompanyRepoContract 
{

	public function storeCompany($data);
    public function updateCompany($id,$data);
    public function attachEmployees($employees,$company);

}