<?php


namespace System\Company\Filters;

use System\Infrastructure\QueryFilter;

class CompanyFilters extends QueryFilter
{

	public function name($name = '')
	{
		return $this->builder->where('name','LIKE',"%$name%");
	}
}