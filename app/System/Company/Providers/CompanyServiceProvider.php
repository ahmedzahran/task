<?php

namespace System\Company\Providers;

use Illuminate\Support\ServiceProvider;
use System\Company\Contracts\CompanyRepoContract;
use System\Company\Repositories\CompanyRepository;

class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    $this->app->bind(CompanyRepoContract::class,CompanyRepository::class);

    }
}
