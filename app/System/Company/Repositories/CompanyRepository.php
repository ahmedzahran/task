<?php

namespace System\Company\Repositories;
use System\Company\Contracts\CompanyRepoContract;
use System\Company\Models\Company;
use System\Infrastructure\Repository;
use System\User\Repositories\EmployeesRepository;

class CompanyRepository extends Repository implements CompanyRepoContract
{

	protected $model;
    protected $emplRepo;

    public function __construct(Company $company,EmployeesRepository $emp)
    {
        $this->model = $company;
        $this->emplRepo = $emp;
    }


    public function storeCompany($request)
    {
        $company = $this->model->create([
            'name'=>$request->name,
            'tel'=>$request->tel,
            'address'=>$request->address,
            'email'=>$request->email
        ]);

       $filterdEmployees = array_filter($request->employees,'strlen');

       if (count($filterdEmployees)  > 0) 
       {
           
            $this->attachEmployees($filterdEmployees,$company);

       }

        return $company;
    }


    public function updateCompany($id,$request)
    {
    	return $this->getById($id)->update($request->all());
    }


    public function attachEmployees($employees,$company)
    {
        collect($employees)->map(function($emp) use ($company){

            return $this->emplRepo->getById($emp)
                            ->company()
                            ->associate($company->id)
                            ->save();
        });
    }


}