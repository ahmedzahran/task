<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    	return view('welcome');
});

Route::group(['prefix'=>'dashboard','middleware'=>['auth','IsAdmin']],function (){

	Route::get('/','AdminsController@index')->name('admin.index');


	Route::group(['prefix'=>'companies'],function (){
		Route::get('/','CompaniesController@index')->name('companies.index');
		Route::get('create','CompaniesController@create')->name('companies.create');
		Route::get('/{id}','CompaniesController@show')->name('companies.show');
		Route::get('edit/{id}','CompaniesController@edit')->name('companies.edit');

		Route::post('/','CompaniesController@store')->name('companies.store');
		Route::post('attach/employees/{company_id}','CompaniesController@attachEmployeesToCompany')->name('companies.attach.employees');
		Route::delete('delete/{id}','CompaniesController@destroy')->name('companies.destroy');
		Route::patch('update/{id}','CompaniesController@update')->name('companies.update');

	});

	Route::group(['prefix' => 'employees'],function(){

		Route::get('/','EmployeesController@index')->name('employees.index');
		Route::get('create','EmployeesController@create')->name('employees.create');
		Route::get('/{id}','EmployeesController@show')->name('employees.show');
		Route::get('edit/{id}','EmployeesController@edit')->name('employees.edit');

		Route::get('detach/company/{company_id}/{employee_id}','EmployeesController@detachEmployee')->name('employees.detach.company');


		Route::post('/','EmployeesController@store')->name('employees.store');
		Route::delete('delete/{id}','EmployeesController@destroy')->name('employees.destroy');
		Route::patch('update/{id}','EmployeesController@update')->name('employees.update');
	});


	Route::group(['prefix'=> 'api'],function(){

		route::get('employees','ApiEmployeesController@index')->name('employees.api.index');

	});


});

Route::group(['prefix'=>'employee','middleware'=>'auth'],function (){

	Route::get('profile','ProfileController@index')->name('employees.profile');

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
